<?php

use FW\Installer\Structures\AppInstall as Installer;

$prompts = array(
    'connection.config' => array(
        'driver' => array(
            'prompt' => 'Enter the database driver',
            'default' => 'mysql',
            'required' => true
        ),
        'host' => array(
            'prompt' => 'Enter the database host',
            'default' => '127.0.0.1'
        ),
        'port' => array(
            'prompt' => 'Enter the database port',
            'default' => '3306'
        ),
        'database' => array(
            'prompt' => 'Enter the database name',
            'required' => true
        ),
        'username' => array(
            'prompt' => 'Enter the database username'
        ),
        'password' => array(
            'prompt' => 'Enter the database password',
            'hidden' => true
        )
    )
);

return new Installer($prompts);
