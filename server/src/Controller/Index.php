<?php

namespace <%= namespace %>\Controller;

use FW\Router\Standard\AbstractController;

class Index extends AbstractController {

    public function helloAction() {
        echo 'Welcome to FW Kit';
    }
}
