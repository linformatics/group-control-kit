<?php

namespace <%= namespace %>\Controller;

class Restricted extends \GroupControl\Controller\AuthController {

    public function indexAction() {
        $this->auth();

        echo 'You should only see this if you are logged in';
    }

}
