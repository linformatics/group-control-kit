<?php

require __DIR__ . '/../../../../api.php';

$connection = FW::init('<%= shortcode %>', false)->container('module.connection');

return array(
    'paths' => array(
        'migrations' => __DIR__ . '/migrations'
    ),
    'environments' => array(
        'default_migration_table' => '<%= shortcode %>_migration_log',
        'default_database' => 'main',
        'main' => array(
            'name' => $connection->getDbName(),
            'connection' => $connection->getPdo()
        )
    )
);
