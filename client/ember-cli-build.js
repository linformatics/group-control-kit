/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
    var app = new EmberApp(defaults, {
        sassOptions: {
            includePaths: ['node_modules/fw-ember-gc/styles/']
        },
        hinting: false,
        fingerprint: {enabled: false}
    });

    return app.toTree();
};
