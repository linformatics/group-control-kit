import env from '<%= shortcode %>/config/environment';

var apiRoot = env.APP.config.url + env.APP.config.api;

export default function () {
    this.namespace = apiRoot;
}
