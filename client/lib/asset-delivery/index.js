module.exports = {
    name: 'asset-delivery',
    postBuild: function (results) {
        var fs = this.project.require('fs-extra'),
            walkSync = this.project.require('walk-sync'),
            root = this.project.root.replace(/\/?$/, '/'),
            config = fs.readJsonSync(root + '../fw.json'),
            outputDir = ((config.outputDirectory.charAt(0) === '.') ?
                root + '../' + config.outputDirectory :
                config.outputDirectory).replace(/\/?$/, '/'),
            inDir = results.directory,
            assets = walkSync(inDir);

        fs.ensureDirSync(outputDir);

        assets.forEach(function (relativePath) {
            if (relativePath.slice(-1) === '/' || relativePath.match(/test/)) { return; }
            fs.copySync(inDir + '/' + relativePath, outputDir + '/' + relativePath, {clobber: true});
        });
    }
};
