import service from 'ember-service/inject';
import Route from 'ember-route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Route.extend(ApplicationRouteMixin, {
    title: 'Application',

    config: service(),

    actions: {
        reload() {
            window.location.replace(this.get('config.baseURL'));
        }
    }
});
