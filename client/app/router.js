import computed from 'ember-computed';
import service from 'ember-service/inject';
import Router from 'ember-router';
import envConfig from './config/environment';
import authRouter from 'fw-ember/router';

const EmberRouter = Router.extend({
    location: envConfig.locationType,

    config: service(),

    rootURL: computed(function () {
        return this.get('config.url') || envConfig.baseURL;
    })
});

EmberRouter.map(function () {
    // Add your app-specifc routes here

    // Don't remove this as it is needed to setup important routes
    authRouter(this);
});

export default EmberRouter;
