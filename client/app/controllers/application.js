import Controller from 'ember-controller';
import service from 'ember-service/inject';
import ApplicationControllerMixin from 'fw-ember/mixins/application-controller';

export default Controller.extend(ApplicationControllerMixin, {
    session: service()
});
