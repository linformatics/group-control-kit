/* jshint node: true */
/* jscs:disable */
var omit = require('lodash.omit'),
    config = require('./../../fw.json');

config.api = 'api/v' + config.apiVersion + '.php';

module.exports = function (environment) {
    var ENV = {
        modulePrefix: '{{APP_CODE}}',
        environment: environment,
        baseURL: config.url,
        locationType: 'hash',
        EmberENV: {FEATURES: {}},

        APP: {
            name: config.name,
            version: config.version,
            config: omit(config, ['apiVersion', 'outputDirectory', 'dependencies'])
        },
        'ember-cli-mirage': {
            enabled: false
        }
    };

    if (environment === 'development') {
        // ENV.APP.LOG_RESOLVER = true;
        ENV.APP.LOG_ACTIVE_GENERATION = true;
        ENV.APP.LOG_TRANSITIONS = true;
        ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        ENV.APP.LOG_VIEW_LOOKUPS = true;
    }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.baseURL = '/';
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
        ENV['ember-cli-mirage'].enabled = true;
    }

    if (environment === 'production') {
        ENV.baseURL = 'APP_URI';
    }

    return ENV;
};
