/*
 * FW Kit Gulpfile v1.1
 *
 * Contains tasks necessary to setup and build the application.
 *
 * Sections:
 *
 * 1. Setup/Help Tasks
 * 2. Build/Copy Tasks
 * 3. Clean Tasks
 * 4. Testing/Validation Tasks
 * 5. Release Compilation Tasks
 * 6. Miscellaneous/Temporary Tasks
 *
 */
var fs = require('fs'),
    del = require('del'),
    zip = require('gulp-zip'),
    path = require('path'),
    jscs = require('gulp-jscs'),
    file = require('gulp-file'),
    chalk = require('chalk'),
    phpcs = require('gulp-phpcs'),
    slash = require('slash'),
    shell = require('gulp-shell'),
    assign = require('lodash.assign'),
    gulpif = require('gulp-if'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    phplint = require('phplint').lint,
    phpunit = require('gulp-phpunit'),
    replace = require('gulp-replace'),
    sequence = require('run-sequence'),
    gulp = require('gulp-help')(require('gulp'), {
        aliases: ['h'],
        hideEmpty: true,
        hideDepsMessage: true
    }),

    pkg = require('./package.json'),
    configDefaults = {
        apiPath: '../../',
        outputDirectory: './dist/',
        url: '/'
    },
    appPath = slash(path.resolve('./client')),
    jsPath = './client/app/**/*.js',
    shellOpts = {
        cwd: appPath,
        env: {
            PATH: [process.env.PATH, appPath + '/node_modules/.bin'].join(process.platform === 'win32' ? ';' : ':')
        }
    },
    cleanPaths = {
        built: [
            'client/dist/**',
            'client/tmp/**',
            'fw.json',
            'config.json'
        ],
        dependencies: [
            'client/node_modules/**',
            'client/bower_components/**',
            'node_modules/**'
        ]
    },
    config, apiRoot;

try {
    fs.statSync('./config.json');
} catch (e) {
    fs.writeFileSync('./config.json', '{}');
}

config = assign(configDefaults, require('./config.json'));
apiRoot = process.env.API_ROOT || slash(path.resolve(config.apiPath));

config.apiVersion = config.apiVersion ||
    JSON.parse(fs.readFileSync(apiRoot + '/composer.json')).version.substring(0,3);

/*
 * 1. Setup/Help Tasks
 */

gulp.task('init', 'Installs ember-cli npm and bower dependencies', shell.task(['npm install', 'bower install', process.platform === 'win32' ? 'ember windows' : 'echo "Platform is not windows"'], shellOpts));

gulp.task('phinx:migrate', 'Migrates or rolls back the database', shell.task([
    'php ' + apiRoot + '/addons/migration-core/migrate.php'
]));

gulp.task('phinx:rollback', 'Rollsback the database version', shell.task([
    'php ' + apiRoot + '/addons/migration-core/rollback.php'
]));

gulp.task('phinx:create', 'Create a new database migration', shell.task([
    'php ' + apiRoot + '/addons/migration-core/create.php'
]));

/*
 * 2. Build/Copy Tasks
 */

gulp.task('dev', 'Builds the Ember app in the development environment', function (cb) {
    sequence('dist:clean', 'config:build', ['ember:build', 'api:copy'], cb);
});

gulp.task('prod', 'Builds the Ember app in the production environment', function (cb) {
    sequence('dist:clean', 'config:build', ['ember:build:prod', 'api:copy'], cb);
});

gulp.task('watch', 'Runs the app in watch mode', ['api:copy', 'config:build'], shell.task(['ember build --watch'], shellOpts));

gulp.task('ember:build', shell.task(['ember build'], shellOpts));
gulp.task('ember:build:prod', shell.task(['ember build --prod'], shellOpts));

// Copies the api calling file to the output directory
gulp.task('api:copy', function () {
    console.log(chalk.green('Copying api files....'));
    return gulp.src('./server/index.php')
        .pipe(rename({basename: 'v' + config.apiVersion}))
        .pipe(replace('{{api-path}}', apiRoot))
        .pipe(gulp.dest(config.outputDirectory + 'api/'));
});

function getFwOutput(extra) {
    extra = extra || {};
    return assign({
        name: pkg.name.replace(/-/g, ' '),
        programId: pkg.shortcode,
        version: pkg.version,
        apiVersion: config.apiVersion
    }, extra, pkg.fw);
}

gulp.task('config:build', function () {
    var output = getFwOutput({
        url: config.url,
        outputDirectory: config.outputDirectory
    });

    return file('fw.json', JSON.stringify(output), {src: true})
            .pipe(gulp.dest('./'));
});

gulp.task('config:build:release', function () {
    var output = getFwOutput({
        outputDirectory: './public/'
    });

    return file('fw.json', JSON.stringify(output), {src: true})
            .pipe(gulp.dest('./'));
});

/*
 * 3. Clean Tasks
 */

gulp.task('clean', 'Removes all built files and installed dependencies', ['dist:clean'], function () {
    console.log(chalk.green('Removing built files....'));
    del.sync(cleanPaths.built);
    console.log(chalk.green('Removing dependencies....'));
    del.sync(cleanPaths.dependencies);
});

gulp.task('clean:release', function (cb) {
    del('./public/', {force: true}).then(function () {
        cb();
    }).catch(cb);
});

// TODO: Fix for windows
gulp.task('dist:clean', function (cb) {
    if (config.outputDirectory === './' || config.outputDirectory === '/') {
        throw new Error(chalk.red('cannot output to root or home directory'));
    }
    console.log(chalk.blue('Removing old dist files....'));
    del(config.outputDirectory + '**/*', {force: true}).then(function () {
        cb();
    }).catch(cb);
});

/*
 * 4. Testing/Validation Tasks
 */

// 4.1 Linter Tasks
gulp.task('lint', 'Lints all files and checks all files for code style errors',
    ['lint:client', 'lint:server', 'sniff:client', 'sniff:server']);

gulp.task('lint:client', function () {
    return gulp.src(jsPath)
            .pipe(jshint())
            .pipe(jshint.reporter('default'))
            .pipe(jshint.reporter('fail'));
});

gulp.task('lint:server', function (cb) {
    phplint(['./server/src/**/*.php'], {limit: 10}, function (err) {
        if (err) {
            cb(err);
            process.exit(1);
        }
        cb();
    });
});

gulp.task('sniff:client', function () {
    return gulp.src(jsPath)
            .pipe(jscs({configPath: 'client/.jscsrc'}))
            .pipe(jscs.reporter())
            .pipe(jscs.reporter('fail'));
});

gulp.task('sniff:server', function () {
    return gulp.src('./server/src/**/*.php')
        .pipe(phpcs({
            bin: apiRoot + '/vendor/bin/phpcs',
            standard: './standards/fw_phpcs/',
            colors: true
        }))
        .pipe(phpcs.reporter('log'))
        .pipe(phpcs.reporter('fail'));
});

// 4.2 Testing Tasks
gulp.task('test-ember', shell.task(['ember test'], shellOpts));

gulp.task('test:unit:setup', function () {
    return gulp.src('./server/tests/loader.php.dist')
            .pipe(replace('{{VENDOR_PATH}}', apiRoot + '/vendor/autoload.php'))
            .pipe(rename('loader.php'))
            .pipe(gulp.dest('./server/tests/'));
});

gulp.task('test-unit', ['test:unit:setup'], function () {
    return gulp.src('phpunit.xml')
        .pipe(phpunit(apiRoot + '/vendor/bin/phpunit', {debug: false, notify: true, silent: true}));
});

gulp.task('validate', ['config:build'], function (cb) {
    sequence('lint', 'test-unit', /*'test-ember', */cb);
});

/*
 * 5. Release Compilation Tasks
 */
gulp.task('zip', function () {
    var fileName = pkg.shortcode.toLowerCase() + '-' + pkg.version;
    return gulp.src([
     '!./server/config/local.php',
     '!./server/.*',
     '!./server/tests/**',
     '!./server/tests',
     './server/**/**',
     './public/**/**',
     './content/**/**',
     'fw.json',
     'install.php',
     'update.php'
    ], {base: './'})
     .pipe(gulpif(function (file) {
         return /\.dist$/i.test(file.path);
     }, rename({extname: ''})))
     .pipe(zip(fileName + '.zip'))
     .pipe(gulp.dest('./'));
});

gulp.task('release', 'Compiles and zips the code for release', function (cb) {
    sequence('clean:release', 'config:build:release', 'ember:build:prod', 'zip', cb);
});

/*
 * 6. Miscellaneous/Temporary Tasks
 */
